#!/usr/bin/env python
import argparse
import os
import json
from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager
from ansible.inventory.manager import InventoryManager

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Get hosts from patterns')
    parser.add_argument('--inventory_path', action="store")
    parser.add_argument('--patterns_path', action="store")
    params = vars(parser.parse_args())
    # print(params)
    json_data = {}
    with open(params['patterns_path']) as f:
        json_data = json.load(f)

    loader = DataLoader()
    inventory = InventoryManager(loader=loader, sources=params['inventory_path'])
    result = {"runs": [], "all_hosts": [h.name for h in inventory.get_hosts()]}

    for content in json_data:
        result["runs"].append(
            {"hosts": [h.name for h in inventory.get_hosts(content["pattern"])], "pattern": content["pattern"],
             "tags": content["tags"]})

    json_object = json.dumps(result, indent=4)

    print json_object
