def call(args = [:], Closure body = null) {

    //Set Default Args
    args = [
            triggeredJobsWithParameters : [
                    [
                            triggeredJob: '',
                            triggeredParameters: [],
                            description: ""
                    ]
            ],
            parallelAction: false
    ] << args

    assert args.triggeredJobsWithParameters[0].triggeredJobs != []

//    if(body){
//        body.resolveStrategy = Closure.DELEGATE_FIRST
//        body.delegate = args
//    }

    //You can call body wherever you want in this function, but after args initialization
    //body()

    // Clean workspace before doing anything
    //deleteDir()
    //Helper helper = new Helper(this)

    if (! args.parallelAction){
        echoStage('Run dependency Jobs'){
            args.triggeredJobsWithParameters.each{
                def jobName = (it.triggeredJob =~ '.*\\/(.*)')[0][1]
                echoStage("Run ${jobName} ${it.description}"){
                    build job: it.triggeredJob, parameters: it.triggeredParameters
                }
            }
        }
    }else{
        echoStage('Parallel Run dependency Jobs'){
            def parallel_steps = [:]
            args.triggeredJobsWithParameters.each{
                def jobName = (it.triggeredJob =~ '.*\\/(.*)')[0][1]
                parallel_steps[("Run ${jobName} ${it.description}")] = { build job: it.triggeredJob, parameters: it.triggeredParameters }
            }
            try {
                parallel parallel_steps
            } catch (exception) {
                throw (exception)
            }
        }
    }
}
