import jobrunner.Common
def call(args = [:], common) {

    //Set Default Args
    args = [
            zone            : '',
            inventoryProject: '',
            inventoryBranch : 'master',
            inventorySubdir : 'inventory',
            inventoryPath   : '',
            // Path to project and their settings
            groupVarsProject: 'inventory/group_vars_all.git',
            groupVarsBranch : 'master',
            useTags: false,
            credentialId: null

    ] << args

    def groupVarsSubdir = "group_vars"
    def result = [:]


    // Clean workspace before doing anything
    //deleteDir()
    try {


        stage('Clone Inventory') {
            gitInventoryRepo = common.gitDefaultCheckout(args.inventoryProject, args.inventoryBranch, args.inventorySubdir, args.credentialId, true, args.useTags)
            result << ["${args.inventoryProject}": ["branch": args.inventoryBranch, "commit": gitInventoryRepo.GIT_COMMIT]]
        }

        stage('Clone General Group Vars') {
            gitGroupVarsRepo = common.gitDefaultCheckout(args.groupVarsProject, args.groupVarsBranch, groupVarsSubdir, args.credentialId, true, false)
            result << ["${args.groupVarsProject}": ["branch": args.groupVarsBranch, "commit": gitGroupVarsRepo.GIT_COMMIT]]

            sh "[ -d ./${args.inventorySubdir}/${args.zone}/group_vars ] || mkdir ./${args.inventorySubdir}/${args.zone}/group_vars;"
            sh "[[ -d ./${args.inventorySubdir}/group_vars/ && \"`find ./${args.inventorySubdir}/ -type f -not -path '*/\\.*' | grep group_vars | grep -v grep | grep -v ./${args.inventorySubdir}/group_vars`\" == '' ]] && mv ./${args.inventorySubdir}/group_vars/* ./${args.inventorySubdir}/${args.zone}/group_vars && rm -rf ./${args.inventorySubdir}/group_vars || echo 'Skip move group_vars'"
            sh "[ -d ./${groupVarsSubdir}/${args.zone}/ ] && cp ./${groupVarsSubdir}/${args.zone}/all.yml ./${args.inventorySubdir}/${args.zone}/group_vars || echo 'Skip move group_vars'"

        }

    } catch (exception) {
        def message = "PIPELINE ABORTED! ${exception.toString()}: ${exception.getMessage()}"
        common.abortPipeline(message, true, false)
    }

    return result
}
