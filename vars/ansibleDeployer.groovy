import jobrunner.Common

def call(args = [:], Common common = null) {
    args = [
            password          : '',
            zone              : '',
            inventoryProject  : '',
            inventoryBranch   : '',
            inventorySubdir   : 'inventory',
            inventoryPath     : '',
            useTags: false,
            groupVarsProject: 'inventory/group_vars_all.git',
            groupVarsBranch : 'master',
            playbook          : '',
            limits            : '',
            tags              : '',
            verbose           : 'v',
            forks             : '5',
            extras            : [],
            secretExtraVars   : [:],
            jobName           : '',
            node              : '',
            useAutodeploySh: true,
            notify: true,
            ansibleRolesProject: '',
            ansibleRolesBranch: '',
            ansibleRolesSubdir: '',
            ticketNumber: '',
            docker: [
                    dockerInside : false,
                    image       : '',
                    bindMounts: []
            ],
            git: [
                    user: '',
                    email: '',
                    CredentialsId: null
            ]
    ] << args

    // Инициализируем переменные
    if (!common) {
        common = new Common(this)
    }

    def deploy_result = [:]
    def inventory_info = [:]
    def vault_args = [:]

    String rootWorkSpace = ''

    //assert args.extras != []

    try {
        deployNotify([
                zone              : args.zone,
                inventoryProject  : args.inventoryProject,
                inventoryBranch   : args.inventoryBranch,
                inventoryPath     : args.inventoryPath,
                limits            : args.limits,
                tags              : args.tags,
                jobName           : args.jobName,
                extras            : args.extras,
                ticketNumber      : args.ticketNumber
        ], args.notify, common) {
            node(args.node) {
                // Используем тайм стэмп для логирования времени выполнения операций
                wrap([$class: 'TimestamperBuildWrapper']) {
                    wrap([$class: 'MaskPasswordsBuildWrapper', varPasswordPairs: [[password: args.password, var: 'args.password']]]) {
                        rootWorkSpace = pwd()
                        def login = BUILD_LOGIN
                        def hostname = sh(script: 'echo -n "$(hostname)"', returnStdout: true)
                        currentBuild.displayName = "#${currentBuild.getNumber()}_${login}"
                        currentBuild.description = "Build number: ${currentBuild.getNumber()}, BuildUser: ${login}<br>Runninng on: ${args.node}:${hostname}; rootWorkSpace=${rootWorkSpace}"

                        if (args.password.size() > 0 || args.secretExtraVars.size() > 0) {
                            vault_args = ['ansible_user': "'${login}'", 'ansible_password': "'${args.password}'"] << args.secretExtraVars
                        }

                        Boolean dryRunMode = ((args.tags.find{ it.contains('--check')} != null) || (args.extras.find{ it.contains('--check')} != null))

                        // Загрузка и проверка Inventory
                        inventory_info = inventory(
                                [
                                        zone            : args.zone,
                                        inventoryProject: args.inventoryProject,
                                        inventoryBranch : args.inventoryBranch,
                                        inventorySubdir : args.inventorySubdir,
                                        inventoryPath   : args.inventoryPath,
                                        groupVarsProject: args.groupVarsProject,
                                        groupVarsBranch: args.groupVarsBranch,
                                        password:  args.password,
                                        useTags: args.useTags,
                                        credentialId: args.git.CredentialsId

                                ], common)


                        deploy_result = deploy(
                                [
                                        ansibleRolesProject: args.ansibleRolesProject,
                                        ansibleRolesBranch: args.ansibleRolesBranch,
                                        ansibleRolesSubdir: args.ansibleRolesSubdir,
                                        zone             : args.zone,
                                        inventoryProject : args.inventoryProject,
                                        inventoryBranch  : args.inventoryBranch,
                                        inventorySubdir  : args.inventorySubdir,
                                        inventoryPath    : args.inventoryPath,
                                        limits           : args.limits,
                                        playbooks        : [playbook: args.playbook, tags: args.tags],
                                        vault_args       : vault_args,
                                        useAutodeploySh  : args.useAutodeploySh,
                                        ansibleVerbose   : args.verbose,
                                        forks            : args.forks,
                                        deployFromArti   : args.deployFromArti,
                                        extras           : args.extras,
                                        jobName          : args.jobName,
                                        docker: args.docker,
                                        credentialId: args.git.CredentialsId
                                ], common)

                        saveState(
                                [
                                        zone           : args.zone,
                                        inventoryProject: args.inventoryProject,
                                        inventoryBranch : args.inventoryBranch,
                                        inventorySubdir : args.inventorySubdir,
                                        dryRunMode: dryRunMode,
                                        play_tags: deploy_result["all_play_tags"],
                                        git: args.git
                                ], common)


                    }
                }
            }
        }

    } catch (exception) {
        throw exception
    }
}
