#!/usr/bin/groovy
import groovy.json.JsonOutput
import workflower.Helper
import workflower.AnsiblePlaybokContent

def call(args = [:]) {

    //Set Default Args
    args = [
            node               : 'master',
            debugVerbosity     : true,
            zones              : [:],
            schemaPath         : "workflower/Helper/workflowCDSchemaTemplate.json",
            inventory_repo     : "",
            branch             : "master",
            credentialsId      : null,
            ansibleRolesProject: '/ansible/ansible-downloader.git',
            ansibleRolesBranch : 'master',
            ansibleRolesSubdir : 'ansible_deploy',
            playbook           : "playbook.yml",
            password: '',
            jobsRootDir: '',
            runZone: '',

    ] << args

    assert args.zones != null && args.zones != []

    def defenition
    def currentDir
    def workflow
    def runs = [:]
    Helper helper = new Helper(this)

    try {
        defenition = workflowCheck(
                [
                        node               : args.node,
                        debugVerbosity     : args.debugVerbosity,
                        zones              : args.zones,
                        schemaPath         : args.schemaPath,
                        inventory_repo     : args.inventory_repo,
                        branch             : args.branch,
                        credentialsId      : args.credentialsId,
                        ansibleRolesProject: args.ansibleRolesProject,
                        ansibleRolesBranch : args.ansibleRolesBranch,
                        ansibleRolesSubdir : args.ansibleRolesSubdir,
                        playbook           : args.playbook
                ]
        )

        stage("Execute jobs"){
            def runCollection = []
            for (step in defenition.stages.find{ it["zone"] == args.runZone}["steps"]) {
                def triggeredJobsWithParameters = []
                for (build in step["builds"]) {
                    def limits = build.limits.include.join(",")
                    if (build.limits.exclude && build.limits.exclude.size() > 0) {
                        def exclude_limits = build.limits.exclude.join(",!")
                        if (limits != '') {
                            limits += ",!${exclude_limits}"
                        } else {
                            limits = "!${exclude_limits}"
                        }
                    }

                    def tags = build.tags.include.join(",")
                    if (build.tags.exclude && build.tags.exclude.size() > 0) {
                        def skip_tags = build.tags.exclude.join(",")
                        if (tags != '') {
                            tags += " --skip-tags ${skip_tags}"
                        } else {
                            tags = "--skip-tags ${skip_tags}"
                        }
                    }
                    triggeredJobsWithParameters.add([
                            "triggeredJob": "${args.jobsRootDir}/${args.zones[args.runZone]["label"]}/PRODUCTS/${defenition.jobName}",
                            "description": "with parameters: limits: '${limits}', tags: '${tags}'",
                            "triggeredParameters":[
                                    helper.getDeployParameter("PASSWORD", args.password),
                                    helper.getDeployParameter("LIMITS", limits),
                                    helper.getDeployParameter("ZONE", args.runZone),
                                    helper.getDeployParameter("FORKS", build.forks.toString()),
                                    helper.getDeployParameter("BRANCH", args.branch),
                                    helper.getDeployParameter("TAGS", tags)
                            ]
                    ])
                }
                runCollection.add( ["triggeredJobsWithParameters": triggeredJobsWithParameters, parallelAction: step["parallel"]] )
            }

            runCollection.each { run ->
                triggeredJobs(run)
            }

        }

    } catch (exception) {
        helper.abortPipeline(exception.getMessage(), true)
    }

}
