import jobrunner.Common

def call(args = [:], Common common = null) {

    //Set Default Args
    args = [
            zone                 : '',                        //Get from some config???
            inventorySubdir      : 'inventory',
            inventoryPath        : '',
            absoluteInventoryPath: '',
            limits               : '',

            //Roles
            ansibleRolesProject  : '',
            ansibleRolesBranch   : '',
            ansibleRolesSubdir   : '',

            //Project from check and generate inventories
            // playbook and tags
            playbooks            : [playbook: '', tags: ''],
            vault_args           : [:],

            // Enable if use auto-deploy.sh, auto-rollback.sh and auto-success.sh
            useAutodeploySh      : true,
            // Verbosity
            ansibleVerbose       : '',
            forks                : '5',
            // Become method
            become               : true,
            // add extras keys
            extras               : [],
            // Skipping cloning artifact and dowload roles
            skipClone            : false,
            jobName              : 'Deploy',
            skipDeploy           : false,
            docker: [
                    dockerInside : false,
                    image       : '',
                    bindMounts: []
            ],
            credentialId: null
    ] << args

    if (!common) {
        common = new Common(this)
    }

    //You can call body elsewhere (check if it is not null before!)
    //body()
    Boolean colorizeError = true
    def result = [:]
    ArrayList callBackWhitelist = []
    def rootWorkSpace = pwd()
    String groupVarsSubdir = "group_vars"
    String srcGroupVarsPath = "${rootWorkSpace}/${groupVarsSubdir}/${args.zone}"
    String dstGroupVarsPath = (args.absoluteInventoryPath.length() > 0) ? '' : "${rootWorkSpace}/${args.inventorySubdir}/${args.zone}/${groupVarsSubdir}"
    String groupVarsFile = 'all.yml'
    String inventoryPath = (args.absoluteInventoryPath.length() > 0) ? "${args.absoluteInventoryPath}" : "${args.inventorySubdir}/${args.zone}/${args.inventoryPath}"
    String inventoryEV = "${rootWorkSpace}/${args.inventorySubdir}/extra_vars.yml"
    if (fileExists(inventoryEV)) {
        args.extras << "-e @${inventoryEV}"
    }

    String ansibleVerbose = (args.ansibleVerbose == 'none') ? '' : args.ansibleVerbose


    Boolean dryRunMode = (args.playbooks.tags.find { it.contains('--check') } != null)

    //Find extras keys in tags and replace it
    if (args.playbooks.tags =~ /^-.*/) {
        args.extras << args.playbooks.tags
        args.playbooks.tags = ''
    }

    // Clean workspace before doing anything
    // deleteDir()
    try {
        if (args.skipClone == false) {
            stage("Clone roles to ${args.jobName}") {
                common.gitDefaultCheckout(args.ansibleRolesProject, args.ansibleRolesBranch, args.ansibleRolesSubdir, args.credentialId, true, false)
                dir(args.ansibleRolesSubdir) {
                    runAnsiblePlaybook(
                            [
                                    ansibleEnv      : ['ANSIBLE_STDOUT_CALLBACK=skippy'],
                                    playbookFile    : 'load_installer.yml',
                                    vault_args      : args.vault_args,
                                    srcGroupVarsPath: srcGroupVarsPath,
                                    dstGroupVarsPath: dstGroupVarsPath,
                                    groupVarsFile   : groupVarsFile,
                                    ansibleVerbose  : ansibleVerbose,
                                    tags            : "load",
                                    limits          : args.limits,
                                    extras          : args.extras,
                                    stageName       : "Load Artifacts",
                                    getFromFile     : false,
                                    inventoryPath   : inventoryPath
                            ],
                            common
                    )
                }
            }

        }

        // Получаем тэги текущего инсталлятора
        try {
            dir(args.ansibleRolesSubdir) {
                result["all_play_tags"] = searchTagsFromPlaybook([
                        playbookFile    : args.playbooks.playbook,
                        getFromFile     : args.useAutodeploySh,
                        tags            : args.playbooks.tags,
                        inventoryPath   : "../${inventoryPath}",
                        limits          : args.limits,
                        extras          : args.extras,
                        vault_args      : args.vault_args,
                        srcGroupVarsPath: srcGroupVarsPath,
                        dstGroupVarsPath: dstGroupVarsPath,
                        groupVarsFile   : groupVarsFile
                ], common)
            }
        } catch (exception) {
            println exception.getMessage()
            result["all_play_tags"] = []
        }

        println result["all_play_tags"]

        def currentDeployAction = ''
        if (result["all_play_tags"].contains("stop")) {
            currentDeployAction = "stop"
        }
        if (result["all_play_tags"].contains("start")) {
            currentDeployAction = "start"
        }

        dir(args.ansibleRolesSubdir) {
            String callBackWhitelistStr = (callBackWhitelist.size() > 0) ? callBackWhitelist.join(',') : ''
            runAnsiblePlaybook(
                    [
                            ansibleEnv      : ["ANSIBLE_CALLBACK_WHITELIST=${callBackWhitelistStr}"],
                            playbookFile    : args.playbooks.playbook,
                            vault_args      : args.vault_args,
                            srcGroupVarsPath: srcGroupVarsPath,
                            dstGroupVarsPath: dstGroupVarsPath,
                            groupVarsFile   : groupVarsFile,
                            ansibleVerbose  : ansibleVerbose,
                            tags            : args.playbooks.tags,
                            limits          : args.limits,
                            extras          : args.extras,
                            become          : args.become,
                            forks           : args.forks,
                            stageName       : "Run Installer Playbooks ${args.jobName}",
                            getFromFile     : args.useAutodeploySh,
                            inventoryPath   : inventoryPath,
                            docker    : args.docker,
                    ], common)
        }
        if( result["all_play_tags"].findAll{["deploy","configure"].contains(it)}.size()>0)
        {
            stage("Tests"){
                println "Run tests steps..."
            }
        }
        

    } catch (exception) {
        String message = "PIPELINE ABORTED!\n${exception.toString()}:\n${exception.getMessage()}\n\nSTACK_TRACE:\n${exception.getStackTrace().join('\n')}"
        common.abortPipeline(message, true, colorizeError)
    }
    return result
}
