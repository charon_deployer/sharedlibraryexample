import jobrunner.Common

def call(def args = [:], Boolean notify = true, Common common = null, Closure body = null) {
    args = [
            zone              : '',
            inventoryProject  : '',
            inventoryBranch   : '',
            inventoryPath     : '',
            limits            : '',
            tags              : '',
            jobName           : '',
            extras            : [],
            BUILD_LOGIN       : '',
            currentBuild      : null,
            ticketNumber: ''
    ] << args

    // Убираем пароль из env переменных
    env.PASSWORD = ''

    //Миграция currentBuild в основные агрументы, для возможности доступа к ним со стороны пайплайна
    args.currentBuild = currentBuild


    println '###############PRE_DEBUG_NOTIFY#############'
    println "Pre init scripts"
    println '###############PRE_DEBUG_NOTIFY#############'

    try {

        body.resolveStrategy = Closure.DELEGATE_FIRST
        body.delegate = args
        body.call()

        currentBuild.result = 'SUCCESS'

    } catch (org.jenkinsci.plugins.workflow.steps.FlowInterruptedException e) {
        currentBuild.result = 'ABORTED'
        throw e
    } catch (exception) {
        currentBuild.result = 'FAILURE'
        throw exception
    } finally {
        println '###############POST_DEBUG_NOTIFY#############'
        println "Post run scripts"
        println '###############POST_DEBUG_NOTIFY#############'
    }

}
