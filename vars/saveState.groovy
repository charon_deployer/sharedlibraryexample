import jobrunner.Common

def call(args = [:], Common common = null) {
    args = [
            zone           : '',
            inventoryProject: '',
            inventoryBranch : '',
            inventorySubdir : '',
            play_tags: [],
            dryRunMode: false,
            git: [user: '', email: '', CredentialsId: null]
    ] << args

    // Инициализируем переменные
    if (!common) {
        common = new Common(this)
    }

    def rootWorkSpace = pwd()
    def inventoryDir = "${rootWorkSpace}/${args.inventorySubdir}/"

    try {
        stage('Upload invetory state') {
            echo('Step 1. Check Tags')
            args.play_tags.findAll{["deploy","configure"].contains(it)}
            if (args.play_tags.size() == 0) {
                echo('Skip this step...')
            } else {
                echo('Step 2. Check Tags')
                dir(inventoryDir){
                    sh("git config user.email '${args.git.email}'; git config user.name '${args.git.user}'")
                    sh("git clean -fdx; git reset --hard")
                    if (! args.git.CredentialsId || args.git.CredentialsId == null){
                        sh("git fetch --tags")
                    }else{
                        sshagent(credentials: [args.git.CredentialsId]) {
                            //sh('git status')
                            sh("git fetch --tags")
                        }
                    }
                    sh("git tag -fa '${args.zone}' -m 'from deploy on ${args.zone}'")
                    if (! args.git.CredentialsId || args.git.CredentialsId == null){
                        sh("git push --force origin --tags")
                    }else{
                        sshagent(credentials: [args.git.CredentialsId]) {
                            //sh('git status')
                            sh("git push --force origin --tags")
                        }
                    }
                }

            }
        }
    } catch (exception) {
        def message = "PIPELINE ABORTED! ${exception.toString()}: ${exception.getMessage()}"
        common.abortPipeline(message, true)
    }
}
