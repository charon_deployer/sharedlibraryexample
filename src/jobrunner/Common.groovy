package jobrunner

import base.BaseSharedLibrary
import hudson.model.*
import hudson.model.User
import java.security.MessageDigest
import groovy.json.JsonOutput
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream



import com.cloudbees.groovy.cps.NonCPS

class Common extends BaseSharedLibrary {
    /**
     * This constructor sets the Jenkins 'script' class as the local script
     * variable in order to resolve execution steps.(sh, withCredentials, etc)
     * @param script the script object
     */
    Common(script) {
        super(script)
    }

    @NonCPS
    def get_instance_project(instance_project_name) {
        return jenkins.model.Jenkins.getInstance().getAllItems().find { it.getFullName() == instance_project_name }
    }

    @NonCPS
    def get_all_props(instance_project) {
        return instance_project.getProperty(ParametersDefinitionProperty.class).getParameterDefinitions().collect {
            it.name
        }
    }

    @NonCPS
    def getLastBuildUrl(jobFullName) {
        return jenkins.model.Jenkins.getActiveInstance().getItemByFullName(jobFullName).getLastBuild().getAbsoluteUrl()
    }


    /**
     * Run shell command wrapped in XTerm. Step wrapper required!
     * @param args optional arguments (map):
     * bash (string, default: '/bin/bash') - path to bash;
     * echoed (boolean, default: false) - echo command, if true;
     * hidden (boolean, default: true) - add -e key to command, if true
     * unbuffered (boolean, default false) - add -u key (useful for python)
     * @param command the command text
     * @return the result of command execution
     * */
    def shell(Map args = [:], command) {
        def _args = ['bash': '/bin/bash', 'echoed': false, 'hidden': true, 'unbuffered': false] << args

        def bArgs = []
        if (_args.hidden) bArgs << '-e'
        if (_args.unbuffered) bArgs << '-u'

        def bash = "#!${_args.bash}"

        if (bArgs) bash += " ${bArgs.join(' ')}"

        def _command = "${bash}\n${command}"
        this.script.wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm']) {
            if (_args.echoed) this.script.echo(_command)
            def result = this.script.sh(script: _command, returnStdout: true)
            return result
        }
    }


    //gzip with base64 encode - https://gist.github.com/welshstew/3d1fbff954f94182477b
    def zip(String s) {
        def targetStream = new ByteArrayOutputStream()
        def zipStream = new GZIPOutputStream(targetStream)
        zipStream.write(s.getBytes('UTF-8'))
        zipStream.close()
        def zippedBytes = targetStream.toByteArray()
        targetStream.close()
        return zippedBytes.encodeBase64()
    }

    //ungzip with base64 decode - https://gist.github.com/welshstew/3d1fbff954f94182477b
    def unzip(String compressed) {
        def inflaterStream = new GZIPInputStream(new ByteArrayInputStream(compressed.decodeBase64()))
        def uncompressedStr = inflaterStream.getText('UTF-8')
        return uncompressedStr
    }


    /**
     * Creates uuid string
     * @return the uuid string
     * */
    def createUUID() {
        return UUID.randomUUID().toString()
    }

    /**
     * Creates random password using UUID string
     * @return the password string
     * */
    def createPassword() {
        return createUUID().replaceAll('-', '')
    }

    /**
     * Creates vault password file in current dir.
     * dir() step needs to be used in order to set current workdir.
     * @param fileName the name of file, that will be created.
     * @return the name of file
     */
    def ansibleCreateVaultPasswordFile(filename = '') {
        def password = createPassword()
        def passwordFileName = (filename) ? filename : createUUID()
        passwordFileName = ".${passwordFileName}_vault_pass.txt"
        this.script.writeFile file: passwordFileName, text: password
        return passwordFileName
    }

    /**
     * Creates ansible vault file (installed ansible required!)
     * wrap this function in dir() step to set current work dir
     * @param vars - Map object
     * @param fileName - optional name of vault file, can be omitted
     * @param srcPath - path to the group_vars file to encrypt
     * @param dstPath - destanation path to the group_vars encrypted file
     * @param groupVarsFile - filename to the group_vars file
     * @return [dataFile: '', passwordFile: '']
     */
    def ansibleCreateVaultFile(vars, fileName = '', srcPath = '', dstPath = '', groupVarsFile = '', encryptGroupVars = true) {
        def result = [:]
        def currentWorkSpace = this.script.pwd()
        def lowPriorityVars = [
                'ansible_ssh_pass',
                'ansible_sudo_pass',
                'ansible_ssh_user',
                'ansible_user',
                'ansible_password'
        ]
        def removeTimeout = 10
        def vaultFileName = (fileName) ? fileName : createUUID()
        vaultFileName = ".${vaultFileName}_vault.yml"
        def passwordFileName = ansibleCreateVaultPasswordFile()
        def ymlData = ''
        if (srcPath != '' && this.script.fileExists(srcPath) && dstPath != '' && this.script.fileExists(dstPath) && groupVarsFile != '') {
            this.script.sh "[ -d ${srcPath} ] && cp ${srcPath}/${groupVarsFile} ${dstPath} || echo 'Skip copy vault group_vars file'"
            if (encryptGroupVars == true) {
                ymlData = vars.findAll { lowPriorityVars.contains(it.key) == false }.collect {
                    "${it.key}: ${it.value}"
                }.join("\n")
                def groupVarsYmlData = vars.findAll { lowPriorityVars.contains(it.key) == true }.collect {
                    "${it.key}: ${it.value}"
                }.join("\n")
                def currentData = this.script.readFile "${dstPath}/${groupVarsFile}"
                this.script.writeFile file: "${dstPath}/${groupVarsFile}", text: currentData + "\n" + groupVarsYmlData
                //encrypt vault file
                this.shell("ansible-vault encrypt ${dstPath}/${groupVarsFile} --vault-password-file ${passwordFileName}")
            } else {
                ymlData = vars.collect { "${it.key}: ${it.value}" }.join("\n")
            }
        } else {
            ymlData = vars.collect { "${it.key}: ${it.value}" }.join("\n")
        }
        if (ymlData != '') {
            this.script.writeFile file: vaultFileName, text: ymlData
            //encrypt vault file
            this.shell("ansible-vault encrypt ${vaultFileName} --vault-password-file ${passwordFileName}")
            this.script.sh(script: "sleep ${removeTimeout} && rm ${currentWorkSpace}/${vaultFileName} ${currentWorkSpace}/${passwordFileName} & >/dev/null 2>&1", returnStdout: false)
            result = [vaultFile: vaultFileName, passwordFile: passwordFileName]
        } else {
            this.script.sh(script: "sleep ${removeTimeout} && rm ${currentWorkSpace}/${passwordFileName} & >/dev/null 2>&1", returnStdout: false)
            result = [vaultFile: '', passwordFile: passwordFileName]
        }

        return result
    }

    /* unused function */

    def run_ansible_playbook(PLAYBOOK, INVENTORY, LIMITS, VERBOSE, TAGS, EXTRA_VARS) {
        this.script.wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm']) {
            this.script.ansiblePlaybook(
                    playbook: PLAYBOOK,
                    inventory: INVENTORY,
                    extras: "-$VERBOSE$LIMITS -t $TAGS $EXTRA_VARS")
        }
    }

    /**
     * Run single ansible playbook.
     * @param args - optional parameters:
     * verbose - ansible verbosity level (empty string);
     * extraVars - map of extra variables (empty map);
     * limit - limits (hosts or groups) (empty strings);
     * tags - the playbook's tags (empty string);
     * vault - map [vaultFile: 'path/to/file', passwordFile: 'path/to/file'] (empty strings);
     * colorized - colorize ansible's output (false)
     * become - add -b key to --extra-vars if true (false)
     * forks - number of forks (5)
     * Hint:
     * args.extraVars: [
     *       login: 'mylogin',
     *       secret_key: [value: '%#547', hidden: true]
     * ])
     * Use hidden key in extraVars map to keep the variable secret in the build log
     * @param pathPlaybook the path to playbook
     * @param pathInventory the path to inventory
     */
    def ansibleRunSinglePlaybook(Map args = [:], pathPlaybook, pathInventory) {
        args = [verbose: '', extrasKeys: [], extraVars: [:], limit: '', tags: '', vault: [vaultFile: '', passwordFile: ''], forks: 5, colorized: true, become: false] << args
        def inventory = ''
        def extra_vars = ''
        def extras = []
        extras = extras + args.extrasKeys
        if (args.become) extras << '-b'
        if (args.verbose) extras << "-${args.verbose}"
        if (args.vault.vaultFile != '' && args.vault.passwordFile) {
            extras << "--extra-vars @${args.vault.vaultFile} --vault-password-file ${args.vault.passwordFile}"
        }
        if (args.vault.vaultFile == '' && args.vault.passwordFile) {
            extras << "--vault-password-file ${args.vault.passwordFile}"
        }
        def spec = [
                playbook : pathPlaybook,
                inventory: pathInventory,
                limit    : args.limit,
                tags     : args.tags,
                extraVars: args.extraVars,
                extras   : (extras) ? extras.join(' ') : '',
                forks    : args.forks,
                colorized: args.colorized
                /*
                colorized: true,
                credentialsId : '',
                dynamicInventory : false,
                forks : 5,
                installation : '',
                inventoryContent : '',
                skippedTags : '',
                startAtTask : '',
                sudo : true,
                sudoUser : '',
                tags : tags
                */
        ]
        //this.script.echo(extras.toString())
        this.script.wrap([$class: 'AnsiColorBuildWrapper', colorMapName: "xterm"]) {
            this.script.ansiblePlaybook(spec)
        }
    }

    /**
     * Run single ansible action.
     * @param args - optional parameters:
     * verbose - ansible verbosity level (empty string);
     * extraVars - map of extra variables (empty map);
     * limit - limits (hosts or groups) (empty strings);
     * vault - map [vaultFile: 'path/to/file', passwordFile: 'path/to/file'] (empty strings);
     * colorized - colorize ansible's output (false)
     * become - add -b key to --extra-vars if true (false)
     * forks - number of forks (5)
     * Hint:
     * args.extraVars: [
     *       login: 'mylogin',
     *       secret_key: [value: 'g4dfKWENpeF6pY05', hidden: true]
     * ])
     * Use hidden key in extraVars map to keep the variable secret in the build log
     * @param pathPlaybook the path to ansiblecfg dir
     * @param pathInventory the path to inventory
     */
    def runAnsibleAction(Map args = [:], pathInventory) {
        args = [verbose: '', extrasKeys: [], extraVars: [:], limit: '', vault: [vaultFile: '', passwordFile: ''], forks: 5, colorized: true, become: false] << args
        def inventory = ''
        def extra_vars = ''
        def extras = []
        extras = extras + args.extrasKeys
        if (args.extraVars) {
            extra_vars = args.extraVars.collect { "-e '${it.key}=${it.value}'" }.join(' ')
        }
        if (args.become) extras << '-b -e become=yes'
        if (args.verbose) extras << "-${args.verbose}"
        if (args.vault.vaultFile != '' && args.vault.passwordFile) {
            extras << "--extra-vars @${args.vault.vaultFile} --vault-password-file ${args.vault.passwordFile}"
        }
        if (args.vault.vaultFile == '' && args.vault.passwordFile) {
            extras << "--vault-password-file ${args.vault.passwordFile}"
        }
        def spec = [
                inventory: pathInventory,
                limit    : (args.limit) ? " -l ${args.limit}" : '',
                extraVars: (extra_vars) ? " ${extra_vars}" : '',
                extras   : (extras) ? " ${extras.join(' ')}" : '',
                forks    : args.forks,
        ]
        //this.script.echo(extras.toString())
        this.script.wrap([$class: 'AnsiColorBuildWrapper', colorMapName: "xterm"]) {
            this.script.sh("ansible all -i ${spec.inventory}${spec.limit}${spec.extraVars}${spec.extras} -f ${spec.forks}");
        }
    }

    /**
     * Serialize matcher
     */
    @NonCPS
    def matchInVars(vars, pattern) {
        def res = (vars =~ pattern)
        return res.collect { it }
    }

    /**
     * Evaluate string variable, from dependency binding variables
     * @param property - map object with dependent parameters for the string to be converted
     * @param value - string string to be converted
     */
    @NonCPS
    def noncpsEvaluate(Map property = [:], value) {
        def sharedData = new Binding()
        def shell = new GroovyShell(sharedData)
        property.each { k, v ->
            sharedData.setProperty(k, v)
        }
        return shell.evaluate('return "' + value + '"').toString()
    }


    //crypt vars from XOR https://stackoverflow.com/questions/33529103/simply-xor-encrypt-in-javascript-and-decrypt-in-java
    @NonCPS
    def decryptStringWithXORFromHex(String input, String key) {
        def res = new StringBuilder();

        while (key.length() < input.length() / 2) {
            key += key;
        }

        for (int i = 0; i < input.length(); i += 2) {
            String hexValueString = input.substring(i, i + 2)
            int value1 = Integer.parseInt(hexValueString, 16)
            int value2 = key.charAt((i / 2).toInteger())

            int xorValue = value1 ^ value2
            res.append(Character.toString((char) xorValue))

        }
        return res.toString()
    }

    // Get SHA-256 from current user API-token
    @NonCPS
    def get_current_token(currentBuild) {
        def userId = currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserId()
        User id = User.get(userId)
        def currentToken = id.getProperty(jenkins.security.ApiTokenProperty.class).getApiToken()
        MessageDigest digest = MessageDigest.getInstance("SHA-256")
        digest.update(currentToken.bytes)
        def hashKey = new BigInteger(1, digest.digest()).toString(16).padLeft(32, '0')
        return hashKey
    }



    @NonCPS
    def getUrlText(urlAdress, timeout = 1000) {
        def url = new URL(urlAdress)
        return url.getText(connectTimeout: timeout)
    }

    @NonCPS
    def getBuildUser(userId) {
        User id = User.get(userId)
        return id.getFullName()
    }
    /*
    @NonCPS
    def getBuildUser() {
        return this.script.currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserName()
    }
     */

    /*
    * Функция предназначена для проверки секретных переменных
    * @param key, @param value - ключ и его значение для переменной
    * @param common - иснтанс common-библиотеки
    * @param secretKey - ключ для дешифровки значения переменных
    * @return возвращает результирующий набор переменных, либо ошибку,
    *    в случае не соответствия паттернам
    */
    def searchInKeysValues(key,value,secretKey){
        //Инициализируем необходимые паттерны для поиска ошибок в key, value
        def res = [
                result: false,
                listSecretKeys: [
                        'password',
                        'pwd',
                        'syspass',
                        'pass'
                ],
                errorKeysPatterns: /([\^\&\?\!\@\|\\\/\>\<\$\#\%;`'":=,.{}])|(\s)/,
                errorValuesPatterns: /([`].*[`])|(\s)|(['])/
        ]
        //Проверка ключа на наличие ошибок
        def resKeyCollect = matchInVars(key,res.errorKeysPatterns)
        if (resKeyCollect.size() != 0){
            return [key: key, value: "ERROR: Find error in key: \'${key}\'. Key contains errors symbols. Please add correct key"]
        }
        //Проверка ключа на соответствие паттернам
        res.listSecretKeys.each{
            if (key.toLowerCase().contains(it) || key.toLowerCase() == it) {
                //echo('FIND')
                res.result = true
                return true
            }
        }
        if (res.result == false){
            return [key: key, value: "ERROR: Not valid key: \'${key}\'. This key is not permitted to use in deploy extra_vars. Please add correct key"]
        }
        // Расшифровываем значение
        value = decryptStringWithXORFromHex(value, secretKey)
        //Проверка значения на соответствие паттернам
        def resValueCollect = matchInVars(value,res.errorValuesPatterns)
        if (resValueCollect.size() != 0){
            return [key: key, value: "ERROR: Value from key: \'${key}\' contains a non-allowed character. Please add correct value"]
        }

        return [key: key, value: "'${value}'"]
    }


    /*
    * Функция для дешифровки секретных параметров и поиска в них ошибок
    * @param varsObj - map объект, содержащий секретные параметры
    * @param secretKey - ключ для дешифровки значения переменных
    * @param common - инстанс common-библиотеки
    */
    def decryptVars(varsObj,secretKey){
        def retVarsObj = [:]
        //Собираем объект готовых переменных и их ошибок
        varsObj.each{ item ->
            def res = searchInKeysValues(item['name'],item['value'],secretKey)
            retVarsObj[res.key]=res.value
        }
        def errorsVars = [:]
        //Производим проверку и вывод соответсвующих ошибок
        if (retVarsObj.size()>0){
            retVarsObj.each{ k,v ->
                if (v.contains('ERROR')){
                    this.script.echo(v)
                    errorsVars << ["${k}": v ]
                }
            }
            if (errorsVars.size()>0){
                abortPipeline('Find errors from SECRET_EXTRA_VARS parameter: ' + JsonOutput.prettyPrint(JsonOutput.toJson(errorsVars)), true)
            }
        }
        return retVarsObj
    }

}
