package workflower

import base.BaseSharedLibrary

class AnsibleInventoryContent extends BaseSharedLibrary {

    private final String inventoryPath
    private inventoryStruct = [:]

    AnsibleInventoryContent(script, inventoryPath) {
        super(script)
        this.inventoryPath = inventoryPath

    }

    void init() {
        this.inventoryStruct = getInventoryContent()
    }

    def getInventoryContent() {
        def sout = new StringBuilder(), serr = new StringBuilder()

        String command = "ansible-inventory -i ${this.inventoryPath} --list -vvv"
        println command
        def proc = command.execute()
        proc.consumeProcessOutput(sout, serr)
        proc.waitForOrKill(1000)
        println(sout)
        println(serr)
        def infoWarning = sout.substring(0, sout.indexOf("\n{"))
        def inventoryString = sout.substring(sout.indexOf("\n{"))

        def inventory = jsonParse(inventoryString)
        inventory.remove('_meta')
        return inventory
    }

    def getInventoryHosts(String limits) {
        def sout = new StringBuilder(), serr = new StringBuilder()

        String command = "ansible all -i ${this.inventoryPath} -l ${limits} --list-hosts"
        println command
        def proc = command.execute()
        proc.consumeProcessOutput(sout, serr)
        proc.waitForOrKill(20000)
        println(sout)
        def info = sout.substring(0, sout.indexOf("):\n"))
        if (sout && sout != '' && ! serr){
            def hostsString = sout.substring(sout.indexOf("):\n"))
            println(hostsString)

            def res = hostsString.split("\n").collect { it.trim()}.findAll{ it != '' && it != '):' }
            return res
        }else{
            throw new hudson.AbortException(serr)
        }

    }

    def getNested(String grp) {
        //def res = [:]
        def findGrps = this.inventoryStruct.findAll {
            it.value && it.value["children"] && it.value["children"].contains(grp)
        }.collect { k, v -> k }

        def res = [:]
        findGrps.each { k ->
            if (res[k]) {
                res[k]["children"].add(grp)
                res[k]["children"].unique { a, b -> a <=> b }
            } else {
                res << [(k): ["children": [grp]]]
            }
            def bufNext = getNested(k)

            bufNext.each { n, n_data ->
                if (res[n]) {
                    res[n]["children"] += n_data["children"]
                    res[n]["children"] = res[n]["children"].unique { a, b -> a <=> b }
                } else {
                    res << [(n): ["children": n_data["children"]]]
                }
            }

        }

        return res
    }

    def filterInventory(ArrayList hosts) {
        def res = [:]
        this.inventoryStruct.findAll { k, v -> k != "all" && v && v != [:] }.each { k, content ->
            if (content["hosts"]) {
                if (hosts) {
                    def buf_hosts = content["hosts"].findAll { hosts.contains(it) }
                    if (buf_hosts.size() > 0) {
                        res << [(k): ["hosts": buf_hosts]]
                    }
                } else {
                    res << [(k): ["hosts": content["hosts"]]]
                }
            }
        }

        res.collect { k, v -> k }.each { g ->
            def bufNested = getNested(g)
            println "bufNested: ${bufNested}"
            bufNested.each { k, v ->
                println "bufNested: ${res}"
                if (res[k]) {
                    println "res[k]: ${res[k]}"
                    res[k]["children"] += v["children"]
                    res[k]["children"] = res[k]["children"].unique { a, b -> a <=> b }
                } else {
                    res << [(k): ["children": v["children"]]]
                }
            }
        }
        println "res: ${res}"
        def rootGroups = this.inventoryStruct["all"]["children"].findAll { g ->
            !this.inventoryStruct.findAll {
                it.value == [:]
            }.keySet().contains(g)
        }
        def resGroups = res.clone().keySet()
        println "rootGroups: ${rootGroups}"
        println "resgrp: ${resGroups}"
        //res << ["all": ["children": rootGroups.intersect(resGroups)]]
        return res
    }


}