package workflower

import base.BaseSharedLibrary
import workflower.AnsibleInventoryContent

class AnsiblePlaybokContent extends BaseSharedLibrary {

    private final String workdir
    private final String inventoryPath
    private final String rootPlaybookfilename
    private playbookStruct = [:]

    AnsiblePlaybokContent(script, workdir, rootPlaybookfilename, inventoryPath) {
        super(script)
        this.workdir = workdir
        this.rootPlaybookfilename = rootPlaybookfilename
        this.inventoryPath = inventoryPath
    }


    def getPlaybook(filename) {
        return [[("${filename}".toString()): parseYaml("${this.workdir}/${filename}")]]
    }

    def appendIncludes(playbooksList) {
        def result = []
        def tags_include = []
        playbooksList.each { playbook ->
            playbook.each { key, content ->
                content.each { section ->
                    if (section.containsKey("tags")) {
                        if (section["tags"] instanceof String) {
                            tags_include += [section["tags"]]
                        } else {
                            tags_include.add(section["tags"])
                        }
                    }

                    if (section.containsKey("include")) {
                        def buf_scr = this.generatePlaybookStruct(section['include'])
                        if (buf_scr) {
                            def include = this.appendIncludes(buf_scr)
                            include.each { i_items ->
                                if (i_items['tags'].size() > 0) {
                                    i_items['tags'] += tags_include
                                } else {
                                    i_items['tags'] = tags_include
                                }
                                result = result + include
                            }
                        }

                    } else if (section.containsKey("import_playbook")) {
                        def buf_scr = this.generatePlaybookStruct(section['import_playbook'])
                        if (buf_scr) {
                            def include = this.appendIncludes(buf_scr)
                            include.each { i_items ->
                                if (i_items['tags'].size() > 0) {
                                    i_items['tags'] += tags_include
                                } else {
                                    i_items['tags'] = tags_include
                                }
                                result = result + include
                            }

                        }
                    } else {
                        if (! section["name"]){
                            section << ["name": "Unnamed"]
                        }
                        result.add(section << ["tags": tags_include])
                    }
                }
            }
        }

        return result

    }

    void generatePlaybookStruct(playbooks){

        def includesStruct = this.appendIncludes(playbooks)
        AnsibleInventoryContent inventory = new AnsibleInventoryContent(null, this.inventoryPath)
        for (item in includesStruct) {
            if (item["hosts"]){
                item["hosts"] = inventory.getInventoryHosts(item["hosts"])
            }else{
                item["hosts"] = inventory.getInventoryHosts('all')
            }
        }
        this.playbookStruct = includesStruct
    }


    void init() {
        def playbooks = this.getPlaybook(this.rootPlaybookfilename)
        generatePlaybookStruct(playbooks)
    }

    def getRolesSturct(){
        def res = []
        for (play in this.playbookStruct){
            for (role in play["roles"]){
                def tags = role["tags"] + play["tags"]
                res.add(["role_name": role["role"] ,"tags": tags, "play_name": play["name"], "hosts": play["hosts"]])
            }
        }

        return res
    }

//    def getRestartPatterns(ArrayList restart_tags = ["stop", "start", "restart"]) {
//        def all_patterns = []
//        this.playbookStruct.each { play ->
//            def check = false
//            if (play.tags.find { pt -> restart_tags.contains(pt) }) {
//                check = true
//            } else if (play.roles && play.roles.find { pr -> pr.tags.find { restart_tags.contains(it) } }) {
//                check = true
//            } else if (play.tasks && play.tasks.find { pta -> pta.tags.find { restart_tags.contains(pta) } }) {
//                check = true
//            }
//
//            if (check == true) {
//                all_patterns << play.hosts
//            }
//        }
//        return all_patterns.unique { a, b -> a <=> b }
//    }

//    def tags(ArrayList condition = ["stop", "start", "restart"]) {
//        def all_tags = []
//        def other_tags = []
//        this.playbookStruct.each { play ->
//            if (play.roles && play.roles.size() > 0) {
//                play.roles.each { role ->
//                    if (condition) {
//                        if ((condition instanceof ArrayList && role.tags.find {
//                            condition.contains(it)
//                        }) || (!condition instanceof ArrayList && role.tags.find { it == condition })) {
//                            all_tags += role.tags
//                        } else {
//                            other_tags += role.tags
//                        }
//                    } else {
//                        all_tags += role.tags
//                    }
//                }
//            }
//
//            if (play.tasks && play.tasks.size() > 0) {
//                play.tasks.each { task ->
//                    if (condition) {
//                        if ((condition instanceof ArrayList && task.tags.find {
//                            condition.contains(it)
//                        }) || (!condition instanceof ArrayList && task.tags.find { it == condition })) {
//                            all_tags += task.tags
//                        } else {
//                            other_tags += task.tags
//                        }
//                    } else {
//                        all_tags += task.tags
//                    }
//                }
//            }
//            if (condition) {
//                if ((condition instanceof ArrayList && play.tags.find {
//                    condition.contains(it)
//                }) || (!condition instanceof ArrayList && play.tags.find { it == condition })) {
//                    all_tags += play.tags
//                } else {
//                    other_tags += play.tags
//                }
//            } else {
//                all_tags += play.tags
//            }
//        }
//        if (condition) {
//            return all_tags.unique { a, b -> a <=> b } - other_tags
//        } else {
//            return all_tags.unique { a, b -> a <=> b }
//        }
//    }

}