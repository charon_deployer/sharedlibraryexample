package workflower

import base.BaseSharedLibrary
import groovy.json.JsonOutput

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.core.JsonParser
import com.github.fge.jsonschema.exceptions.ProcessingException
import com.github.fge.jsonschema.main.JsonSchemaFactory
import com.github.fge.jsonschema.util.JsonLoader


class Helper extends BaseSharedLibrary {

    /**
     * This constructor sets the Jenkins 'script' class as the local script
     * variable in order to resolve execution steps.(sh, withCredentials, etc)
     * @param script the script object
     */
    Helper(script) {
        super(script)
    }

    @NonCPS
    def templateText(String text, Map binding = [:]) {
        //def engine = new groovy.text.SimpleTemplateEngine()
        String res = new groovy.text.GStringTemplateEngine().createTemplate(text).make(binding).toString()
        return res
    }

    @NonCPS
    def getDeployParameter(key, value) {
        if (key.toUpperCase() in ['RUN_JOB_NAME', 'RUN_JOB_FULLNAME', 'USERNAME']) {
            return []
        } else if (key.toUpperCase() in ['PASSWORD', 'SECRET_EXTRA_VARS']) {
            return [$class: 'com.michelin.cio.hudson.plugins.passwordparam.PasswordParameterValue', name: key, value: value]
        } else {
            return [$class: 'WReadonlyStringParameterValue', name: key, description: '', value: value]
        }
    }

    @NonCPS
    def getStringSchemaObjectFromResource(String srcPath, Boolean template = true, Map args = [:]) {
        try {
            this.script.echo 'Start initialize schema'
            String schema
            if (template) {
                assert args != [:]
                String resource = this.script.libraryResource(srcPath)
                schema = templateText(resource, args)
            } else {
                schema = this.script.libraryResource(srcPath)
            }
            
            return schema.toString() // костыль
        } catch (ex) {
            abortPipeline(ex.toString(), true)
        }
    }

    @NonCPS
    def schemaFromResource(String srcPath, Boolean template = true, Map args = [:]) {
        def jsonSchemaFactory = JsonSchemaFactory.byDefault()
        def mapper = new ObjectMapper()
        def schema = getStringSchemaObjectFromResource(srcPath, template, args)
        JsonParser jsonParserSchema = mapper.getJsonFactory().createJsonParser(schema);
        def schemaJsonNode = mapper.readTree(jsonParserSchema)
        def jsonSchema = jsonSchemaFactory.getJsonSchema(schemaJsonNode)

        return jsonSchema
    }

    @NonCPS
    def validateCDWorkflow(String payload, String srcSchemaPath, Boolean template = true, Map args = [:]) {
        def schema = schemaFromResource(srcSchemaPath, template, args)
        def mapper = new ObjectMapper()

        this.script.echo 'Init object'
        JsonParser jsonParserPayload = mapper.getJsonFactory().createJsonParser(payload);
        def payloadJsonNode = mapper.readTree(jsonParserPayload)
        this.script.echo 'Validate object'
        def report = schema.validate(payloadJsonNode)
        if ( !report.isSuccess() ) {
            def jsonE = ''
            for ( message in report ) {
                jsonE += "$message\n"
            }
            abortPipeline(jsonE, false)
        }
        return jsonParse(payloadJsonNode.toString())
    }


    def writeRunPythonResourceScript(String filepath, String args = '') {
        def currDir = this.script.pwd()
        if (args != '') {
            args = " ${args}"
        }
        final pythonContent = this.script.libraryResource(filepath)
        def file_name = (new File(filepath)).getName()
        File file = new File("${currDir}/${file_name}")
        if (!file.exists()) {
            this.script.writeFile(file: "${currDir}/${file_name}", text: pythonContent)
        }
        return this.script.sh(script: "chmod +x ${file_name} && ./${file_name}${args}", returnStdout: true)
    }

    def getHostsFromStruct(String inventoryPath, def runs) {
        def data = jsonDump(runs)
        def currDir = this.script.pwd()
        this.script.writeFile(file: "${currDir}/runs.json", text: data)
        return writeRunPythonResourceScript("workflower/Helper/get_hosts.py", "--inventory_path '${inventoryPath}' --patterns_path '${currDir}/runs.json'")
    }

}
